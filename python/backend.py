from fastapi import FastAPI, WebSocket

app = FastAPI()

@app.get('/ws')
async def reply(): 
    return 'Hey'

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        try: 
            data = await websocket.receive_text()
            await websocket.send_text(f"Hello from python!: {data}")
        except:
            break
