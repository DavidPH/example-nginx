# Example Nginx config

`/etc/nginx/conf.d/default.conf` 

```
server {
    listen       80;
    listen  [::]:80;
    server_name  www.socket-example.tv socket-example.tv;

    location / {
        proxy_pass http://127.0.0.1:3000;
    }
}

server {
    listen        6969;
    listen   [::]:6969;
    server_name  socket-example.tv;

    location /ws {
		proxy_pass http://127.0.0.1:8000/ws;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "Upgrade";
		proxy_set_header Host $host;
    }
}
```